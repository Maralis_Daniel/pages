/*
Thanks to stack-overflow, best friend of any developer
https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array

OK, let's use Fisher-Yates approach for shuffling elements as better way
for shuffling things. Impl is from
https://javascript.info/array-methods#shuffle-an-array

Thanks for @sairenm from antikvartv twitch stream for noticing
Dude if you are reading that, you could've just created PR with fix as well ;)
*/
function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i

        // swap elements array[i] and array[j]
        // we use "destructuring assignment" syntax to achieve that
        // you'll find more details about that syntax in later chapters
        // same can be written as:
        // let t = array[i]; array[i] = array[j]; array[j] = t
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function choose_n(choices, n) {
    // Shuffle array and select first n options
    // Shuffle here reorders arr in place, so keep it in mind
    // Should be replaced by the same thing that do only n steps
    // And then takes only suffix, but no time to really debug it
    // Maybe will rewrite in the future.
    shuffle(choices)
    return choices.slice(0, n);
}

/*
Function for random heroes generation

Params:
    n_player: int, Amount of heroes to be generated. All heroes are
        from different towns if is_mirror options ins't on
    is_mirror: bool, If that option is specified, all generated heroes
        are from one town
    banned_towns: List[str], List of towns to cross out in generation stage
    banned_heroes: List[str], List of heroes to cross out in generation stage
        by default all heroes (company/normal) are used in generation process
Returns:
    heroes: List[Dict], Number of requested heroes with hero-related information
 */
function h3pick(n_players, is_mirror, banned_towns, banned_heroes) {
    let available_towns = new Set();
    let town_to_heroes = {};
    for (const hero_info of heroes) {
        const town_name = hero_info["class"]["town"];
        if (banned_towns.includes(town_name)) {
            continue;
        }
        if (banned_heroes.includes(hero_info["hero"]["name"])) {
           continue;
        }
        available_towns.add(town_name);
        if (town_to_heroes[town_name] === undefined) {
            town_to_heroes[town_name] = [];
        }
        town_to_heroes[town_name].push(hero_info);
    }

    let available_town_list = Array.from(available_towns);
    if (is_mirror) {
        let town = choose_n(available_town_list, 1)[0];
        return choose_n(town_to_heroes[town], n_players);
    } else {
        let towns = choose_n(available_town_list, n_players);
        let final_heroes = [];
        for (const town of towns) {
            const rand_hero = choose_n(town_to_heroes[town], 1)[0];
            final_heroes.push(rand_hero);
        }
        return final_heroes;
    }
}

/*
Creates banned heroes list from "allowed" and "banned" fields
*/
function get_standard_banned_heroes_list(template_heroes) {
    let banned_heroes = []
    for (let hero of template_heroes["banned"]) {
        banned_heroes.push(hero);
    }
    return banned_heroes
}