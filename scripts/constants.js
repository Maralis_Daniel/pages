'use strict';

const URL_PATTERN = 'https://heroes.thelazy.net/index.php/';

const TOWNS_AND_HEROES = {
    Castle: {
        img: 'https://heroes.thelazy.net/images/6/66/Town_portrait_Castle_small.gif',
        heroes: {
            Christian: {
                img: 'https://heroes.thelazy.net/images/f/f8/Hero_Christian.png',
                type: 'Knight',
                specialty: 'Ballista',
                skills: ['Basic_Leadership', 'Basic_Artillery']
            },
            Edric: {
                img: 'https://heroes.thelazy.net/images/7/7f/Hero_Edric.png',
                type: 'Knight',
                specialty: 'Griffins',
                skills: ['Basic_Leadership', 'Basic_Armorer']
            },
            Orrin: {
                img: 'https://heroes.thelazy.net/images/4/4a/Hero_Orrin.png',
                type: 'Knight',
                specialty: 'Archery',
                skills: ['Basic_Leadership', 'Basic_Archery']
            },
            Sorsha: {
                img: 'https://heroes.thelazy.net/images/1/14/Hero_Sorsha.png',
                type: 'Knight',
                specialty: 'Swordsmen',
                skills: ['Basic_Leadership', 'Basic_Offense']
            },
            Sylvia: {
                img: 'https://heroes.thelazy.net/images/5/53/Hero_Sylvia.png',
                type: 'Knight',
                specialty: 'Navigation',
                skills: ['Basic_Leadership', 'Basic_Navigation']
            },
            Valeska: {
                img: 'https://heroes.thelazy.net/images/2/20/Hero_Valeska_%28HotA%29.png',
                type: 'Knight',
                specialty: 'Archers',
                skills: ['Basic_Leadership', 'Basic_Archery']
            },
            Tyris: {
                img: 'https://heroes.thelazy.net/images/c/c1/Hero_Tyris.png',
                type: 'Knight',
                specialty: 'Cavaliers',
                skills: ['Basic_Leadership', 'Basic_Tactics']
            },
            Lord_Haart: {
                img: 'https://heroes.thelazy.net/images/e/ed/Hero_Lord_Haart_Knight.png',
                type: 'Knight',
                specialty: 'Estates',
                skills: ['Basic_Leadership', 'Basic_Estates']
            },
            Catherine: {
                img: 'https://heroes.thelazy.net/images/5/53/Hero_Catherine.png',
                type: 'Knight',
                specialty: 'Swordsmen',
                skills: ['Basic_Leadership', 'Basic_Offense']
            },
            Roland: {
                img: 'https://heroes.thelazy.net/images/7/7f/Hero_Roland.png',
                type: 'Knight',
                specialty: 'Swordsmen',
                skills: ['Basic_Leadership', 'Basic_Armorer']
            },
            Sir_Mullich: {
                img: 'https://heroes.thelazy.net/images/9/9a/Hero_Sir_Mullich_%28HotA%29.png',
                type: 'Knight',
                specialty: 'Speed',
                skills: ['Advanced_Leadership']
            },
            Beatrice: {
                img: 'https://heroes.thelazy.net/images/0/05/Hero_Beatrice.png',
                type: 'Knight',
                specialty: 'Scouting',
                skills: ['Basic_Leadership', 'Basic_Scouting']
            },
            Adela: {
                img: 'https://heroes.thelazy.net/images/3/32/Hero_Adela.png',
                type: 'Cleric',
                specialty: 'Bless',
                skills: ['Basic_Wisdom', 'Basic_Diplomacy']
            },
            Adelaide: {
                img: 'https://heroes.thelazy.net/images/b/b9/Hero_Adelaide_%28HotA%29.png',
                type: 'Cleric',
                specialty: 'Frost_Ring',
                skills: ['Advanced_Wisdom']
            },
            Caitlin: {
                img: 'https://heroes.thelazy.net/images/5/50/Hero_Caitlin_%28HotA%29.png',
                type: 'Cleric',
                specialty: 'Gold',
                skills: ['Basic_Wisdom', 'Basic_Intelligence']
            },
            Cuthbert: {
                img: 'https://heroes.thelazy.net/images/2/24/Hero_Cuthbert.png',
                type: 'Cleric',
                specialty: 'Weakness',
                skills: ['Basic_Wisdom', 'Basic_Estates']
            },
            Ingham: {
                img: 'https://heroes.thelazy.net/images/d/dd/Hero_Ingham.png',
                type: 'Cleric',
                specialty: 'Monks',
                skills: ['Basic_Wisdom', 'Basic_Mysticism']
            },
            Loynis: {
                img: 'https://heroes.thelazy.net/images/8/86/Hero_Loynis.png',
                type: 'Cleric',
                specialty: 'Prayer',
                skills: ['Basic_Wisdom', 'Basic_Learning']
            },
            Rion: {
                img: 'https://heroes.thelazy.net/images/d/d4/Hero_Rion.png',
                type: 'Cleric',
                specialty: 'First_Aid',
                skills: ['Basic_Wisdom', 'Basic_First_Aid']
            },
            Sanya: {
                img: 'https://heroes.thelazy.net/images/9/93/Hero_Sanya.png',
                type: 'Cleric',
                specialty: 'Eagle_Eye',
                skills: ['Basic_Wisdom', 'Basic_Eagle_Eye']
            }
        }
    },
    Rampart: {
        img: 'https://heroes.thelazy.net/images/d/dc/Town_portrait_Rampart_small.gif',
        heroes: {
            Clancy: {
                img: 'https://heroes.thelazy.net/images/6/62/Hero_Clancy.png',
                type: 'Ranger',
                specialty: 'Unicorns',
                skills: ['Basic_Interference', 'Basic_Pathfinding']
            },
            Ivor: {
                img: 'https://heroes.thelazy.net/images/2/2c/Hero_Ivor.png',
                type: 'Ranger',
                specialty: 'Elves',
                skills: ['Basic_Archery', 'Basic_Offense']
            },
            Jenova: {img: 'https://heroes.thelazy.net/images/e/ed/Hero_Jenova.png', type: 'Ranger', specialty: 'Gold', skills: ['Advanced_Archery']},
            Kyrre: {
                img: 'https://heroes.thelazy.net/images/2/23/Hero_Kyrre.png',
                type: 'Ranger',
                specialty: 'Logistics',
                skills: ['Basic_Archery', 'Basic_Logistics']
            },
            Mephala: {
                img: 'https://heroes.thelazy.net/images/8/86/Hero_Mephala.png',
                type: 'Ranger',
                specialty: 'Armorer',
                skills: ['Basic_Leadership', 'Basic_Armorer']
            },
            Ryland: {
                img: 'https://heroes.thelazy.net/images/0/06/Hero_Ryland.png',
                type: 'Ranger',
                specialty: 'Dendroids',
                skills: ['Basic_Leadership', 'Basic_Diplomacy']
            },
            Thorgrim: {
                img: 'https://heroes.thelazy.net/images/c/ce/Hero_Thorgrim_%28HotA%29.png',
                type: 'Ranger',
                specialty: 'Resistance',
                skills: ['Advanced_Resistance']
            },
            Ufretin: {
                img: 'https://heroes.thelazy.net/images/0/05/Hero_Ufretin.png',
                type: 'Ranger',
                specialty: 'Dwarves',
                skills: ['Basic_Interference', 'Basic_Luck']
            },
            Gelu: {
                img: 'https://heroes.thelazy.net/images/e/e2/Hero_Gelu.png',
                type: 'Ranger',
                specialty: 'Sharpshooters',
                skills: ['Basic_Leadership', 'Basic_Archery']
            },
            Giselle: {
                img: 'https://heroes.thelazy.net/images/e/e4/Hero_Giselle.png',
                type: 'Ranger',
                specialty: 'Interference',
                skills: ['Advanced_Interference']
            },
            Aeris: {
                img: 'https://heroes.thelazy.net/images/7/74/Hero_Aeris.png',
                type: 'Druid',
                specialty: 'Pegasi',
                skills: ['Basic_Wisdom', 'Basic_Scouting']
            },
            Alagar: {
                img: 'https://heroes.thelazy.net/images/4/43/Hero_Alagar.png',
                type: 'Druid',
                specialty: 'Ice_Bolt',
                skills: ['Basic_Wisdom', 'Basic_Sorcery']
            },
            Coronius: {
                img: 'https://heroes.thelazy.net/images/e/ed/Hero_Coronius_%28HotA%29.png',
                type: 'Druid',
                specialty: 'Slayer',
                skills: ['Basic_Wisdom', 'Basic_Scholar']
            },
            Elleshar: {
                img: 'https://heroes.thelazy.net/images/b/b8/Hero_Elleshar.png',
                type: 'Druid',
                specialty: 'Intelligence',
                skills: ['Basic_Wisdom', 'Basic_Intelligence']
            },
            Gem: {
                img: 'https://heroes.thelazy.net/images/9/9f/Hero_Gem_%28HotA%29.png',
                type: 'Druid',
                specialty: 'First_Aid',
                skills: ['Basic_Wisdom', 'Basic_First_Aid']
            },
            Malcom: {
                img: 'https://heroes.thelazy.net/images/2/22/Hero_Malcom.png',
                type: 'Druid',
                specialty: 'Eagle_Eye',
                skills: ['Basic_Wisdom', 'Basic_Eagle_Eye']
            },
            Melodia: {
                img: 'https://heroes.thelazy.net/images/4/4b/Hero_Melodia.png',
                type: 'Druid',
                specialty: 'Fortune',
                skills: ['Basic_Wisdom', 'Basic_Luck']
            },
            Uland: {
                img: 'https://heroes.thelazy.net/images/e/ee/Hero_Uland.png',
                type: 'Druid',
                specialty: 'Cure',
                skills: ['Basic_Wisdom', 'Basic_Ballistics']
            }
        }
    },
    Tower: {
        img: 'https://heroes.thelazy.net/images/3/32/Town_portrait_Tower_small.gif',
        heroes: {
            Fafner: {
                img: 'https://heroes.thelazy.net/images/d/d0/Hero_Fafner.png',
                type: 'Alchemist',
                specialty: 'Nagas',
                skills: ['Basic_Scholar', 'Basic_Interference']
            },
            Iona: {
                img: 'https://heroes.thelazy.net/images/5/5c/Hero_Iona_%28HotA%29.png',
                type: 'Alchemist',
                specialty: 'Genies',
                skills: ['Basic_Scholar', 'Basic_Intelligence']
            },
            Josephine: {
                img: 'https://heroes.thelazy.net/images/6/62/Hero_Josephine.png',
                type: 'Alchemist',
                specialty: 'Golems',
                skills: ['Basic_Mysticism', 'Basic_Sorcery']
            },
            Neela: {
                img: 'https://heroes.thelazy.net/images/8/80/Hero_Neela.png',
                type: 'Alchemist',
                specialty: 'Armorer',
                skills: ['Basic_Scholar', 'Basic_Armorer']
            },
            Piquedram: {
                img: 'https://heroes.thelazy.net/images/4/42/Hero_Piquedram.png',
                type: 'Alchemist',
                specialty: 'Gargoyles',
                skills: ['Basic_Mysticism', 'Basic_Scouting']
            },
            Rissa: {
                img: 'https://heroes.thelazy.net/images/7/7f/Hero_Rissa.png',
                type: 'Alchemist',
                specialty: 'Mercury',
                skills: ['Basic_Mysticism', 'Basic_Offense']
            },
            Thane: {
                img: 'https://heroes.thelazy.net/images/d/d6/Hero_Thane.png',
                type: 'Alchemist',
                specialty: 'Genies',
                skills: ['Advanced_Scholar']
            },
            Torosar: {
                img: 'https://heroes.thelazy.net/images/b/b3/Hero_Torosar.png',
                type: 'Alchemist',
                specialty: 'Ballista',
                skills: ['Basic_Mysticism', 'Basic_Tactics']
            },
            Aine: {
                img: 'https://heroes.thelazy.net/images/c/cc/Hero_Aine.png',
                type: 'Wizard',
                specialty: 'Gold',
                skills: ['Basic_Wisdom', 'Basic_Scholar']
            },
            Astral: {
                img: 'https://heroes.thelazy.net/images/3/3a/Hero_Astral.png',
                type: 'Wizard',
                specialty: 'Hypnotize',
                skills: ['Advanced_Wisdom']
            },
            Cyra: {
                img: 'https://heroes.thelazy.net/images/4/45/Hero_Cyra.png',
                type: 'Wizard',
                specialty: 'Haste',
                skills: ['Basic_Wisdom', 'Basic_Diplomacy']
            },
            Daremyth: {
                img: 'https://heroes.thelazy.net/images/0/0d/Hero_Daremyth.png',
                type: 'Wizard',
                specialty: 'Fortune',
                skills: ['Basic_Wisdom', 'Basic_Intelligence']
            },
            Halon: {
                img: 'https://heroes.thelazy.net/images/e/ec/Hero_Halon.png',
                type: 'Wizard',
                specialty: 'Mysticism',
                skills: ['Basic_Wisdom', 'Basic_Mysticism']
            },
            Serena: {
                img: 'https://heroes.thelazy.net/images/7/77/Hero_Serena.png',
                type: 'Wizard',
                specialty: 'Eagle_Eye',
                skills: ['Basic_Wisdom', 'Basic_Eagle_Eye']
            },
            Solmyr: {
                img: 'https://heroes.thelazy.net/images/9/96/Hero_Solmyr.png',
                type: 'Wizard',
                specialty: 'Chain_Lightning',
                skills: ['Basic_Wisdom', 'Basic_Sorcery']
            },
            Theodorus: {
                img: 'https://heroes.thelazy.net/images/9/99/Hero_Theodorus.png',
                type: 'Wizard',
                specialty: 'Magi',
                skills: ['Basic_Wisdom', 'Basic_Ballistics']
            },
            Dracon: {
                img: 'https://heroes.thelazy.net/images/c/c6/Hero_Dracon.png',
                type: 'Wizard',
                specialty: 'Enchanters',
                skills: ['Advanced_Wisdom']
            }
        }
    },
    Inferno: {
        img: 'https://heroes.thelazy.net/images/e/e5/Town_portrait_Inferno_small.gif',
        heroes: {
            Calh: {
                img: 'https://heroes.thelazy.net/images/7/72/Hero_Calh.png',
                type: 'Demoniac',
                specialty: 'Gogs',
                skills: ['Basic_Archery', 'Basic_Scouting']
            },
            Fiona: {
                img: 'https://heroes.thelazy.net/images/6/60/Hero_Fiona.png',
                type: 'Demoniac',
                specialty: 'Hell_Hounds',
                skills: ['Advanced_Scouting']
            },
            Ignatius: {
                img: 'https://heroes.thelazy.net/images/8/8d/Hero_Ignatius.png',
                type: 'Demoniac',
                specialty: 'Imps',
                skills: ['Basic_Tactics', 'Basic_Interference']
            },
            Marius: {
                img: 'https://heroes.thelazy.net/images/9/92/Hero_Marius_%28HotA%29.png',
                type: 'Demoniac',
                specialty: 'Demons',
                skills: ['Advanced_Armorer']
            },
            Nymus: {
                img: 'https://heroes.thelazy.net/images/d/d2/Hero_Nymus_%28HotA%29.png',
                type: 'Demoniac',
                specialty: 'Pit_Fiends',
                skills: ['Advanced_Offense']
            },
            Octavia: {
                img: 'https://heroes.thelazy.net/images/2/2a/Hero_Octavia.png',
                type: 'Demoniac',
                specialty: 'Gold',
                skills: ['Basic_Scholar', 'Basic_Offense']
            },
            Pyre: {
                img: 'https://heroes.thelazy.net/images/1/16/Hero_Pyre.png',
                type: 'Demoniac',
                specialty: 'Ballista',
                skills: ['Basic_Logistics', 'Basic_Artillery']
            },
            Rashka: {
                img: 'https://heroes.thelazy.net/images/9/93/Hero_Rashka.png',
                type: 'Demoniac',
                specialty: 'Efreet',
                skills: ['Basic_Scholar', 'Basic_Wisdom']
            },
            Xeron: {
                img: 'https://heroes.thelazy.net/images/8/89/Hero_Xeron.png',
                type: 'Demoniac',
                specialty: 'Devils',
                skills: ['Basic_Leadership', 'Basic_Tactics']
            },
            Ash: {
                img: 'https://heroes.thelazy.net/images/a/a2/Hero_Ash.png',
                type: 'Heretic',
                specialty: 'Bloodlust',
                skills: ['Basic_Wisdom', 'Basic_Eagle_Eye']
            },
            Axsis: {
                img: 'https://heroes.thelazy.net/images/f/f2/Hero_Axsis.png',
                type: 'Heretic',
                specialty: 'Mysticism',
                skills: ['Basic_Wisdom', 'Basic_Mysticism']
            },
            Ayden: {
                img: 'https://heroes.thelazy.net/images/3/36/Hero_Ayden.png',
                type: 'Heretic',
                specialty: 'Intelligence',
                skills: ['Basic_Wisdom', 'Basic_Intelligence']
            },
            Calid: {
                img: 'https://heroes.thelazy.net/images/c/cf/Hero_Calid.png',
                type: 'Heretic',
                specialty: 'Sulfur',
                skills: ['Basic_Wisdom', 'Basic_Learning']
            },
            Olema: {
                img: 'https://heroes.thelazy.net/images/e/e7/Hero_Olema_%28HotA%29.png',
                type: 'Heretic',
                specialty: 'Weakness',
                skills: ['Basic_Wisdom', 'Basic_Ballistics']
            },
            Xarfax: {
                img: 'https://heroes.thelazy.net/images/4/40/Hero_Xarfax.png',
                type: 'Heretic',
                specialty: 'Fireball',
                skills: ['Basic_Wisdom', 'Basic_Leadership']
            },
            Xyron: {
                img: 'https://heroes.thelazy.net/images/3/3b/Hero_Xyron.png',
                type: 'Heretic',
                specialty: 'Inferno_(spell)',
                skills: ['Basic_Wisdom', 'Basic_Scholar']
            },
            Zydar: {
                img: 'https://heroes.thelazy.net/images/9/98/Hero_Zydar.png',
                type: 'Heretic',
                specialty: 'Sorcery',
                skills: ['Basic_Wisdom', 'Basic_Sorcery']
            }
        }
    },
    Necropolis: {
        img: 'https://heroes.thelazy.net/images/5/54/Town_portrait_Necropolis_small.gif',
        heroes: {
            Charna: {
                img: 'https://heroes.thelazy.net/images/a/a6/Hero_Charna.png',
                type: 'Death_Knight',
                specialty: 'Wights',
                skills: ['Basic_Necromancy', 'Basic_Tactics']
            },
            Clavius: {
                img: 'https://heroes.thelazy.net/images/6/61/Hero_Clavius.png',
                type: 'Death_Knight',
                specialty: 'Gold',
                skills: ['Basic_Necromancy', 'Basic_Offense']
            },
            Galthran: {
                img: 'https://heroes.thelazy.net/images/7/7e/Hero_Galthran_%28HotA%29.png',
                type: 'Death_Knight',
                specialty: 'Skeletons',
                skills: ['Basic_Necromancy', 'Basic_Armorer']
            },
            Isra: {
                img: 'https://heroes.thelazy.net/images/9/91/Hero_Isra.png',
                type: 'Death_Knight',
                specialty: 'Necromancy',
                skills: ['Advanced_Necromancy']
            },
            Moandor: {
                img: 'https://heroes.thelazy.net/images/4/41/Hero_Moandor.png',
                type: 'Death_Knight',
                specialty: 'Liches',
                skills: ['Basic_Necromancy', 'Basic_Learning']
            },
            Straker: {
                img: 'https://heroes.thelazy.net/images/e/e9/Hero_Straker.png',
                type: 'Death_Knight',
                specialty: 'Walking_Dead',
                skills: ['Basic_Necromancy', 'Basic_Interference']
            },
            Tamika: {
                img: 'https://heroes.thelazy.net/images/0/05/Hero_Tamika.png',
                type: 'Death_Knight',
                specialty: 'Black_Knights',
                skills: ['Basic_Necromancy', 'Basic_Offense']
            },
            Vokial: {
                img: 'https://heroes.thelazy.net/images/4/4a/Hero_Vokial.png',
                type: 'Death_Knight',
                specialty: 'Vampires',
                skills: ['Basic_Necromancy', 'Basic_Artillery']
            },
            Lord_Haart_the_Death_Knight: {
                img: 'https://heroes.thelazy.net/images/2/26/Hero_Lord_Haart_Death_Knight.png',
                type: 'Death_Knight',
                specialty: 'Black_Knights',
                skills: ['Advanced_Necromancy']
            },
            Ranloo: {
                img: 'https://heroes.thelazy.net/images/2/2f/Hero_Ranloo.png',
                type: 'Death_Knight',
                specialty: 'Ballista',
                skills: ['Basic_Necromancy', 'Basic_Artillery']
            },
            Aislinn: {
                img: 'https://heroes.thelazy.net/images/2/25/Hero_Aislinn.png',
                type: 'Necromancer',
                specialty: 'Meteor_Shower',
                skills: ['Basic_Necromancy', 'Basic_Wisdom']
            },
            Nagash: {
                img: 'https://heroes.thelazy.net/images/6/6b/Hero_Nagash.png',
                type: 'Necromancer',
                specialty: 'Gold',
                skills: ['Basic_Necromancy', 'Basic_Intelligence']
            },
            Nimbus: {
                img: 'https://heroes.thelazy.net/images/c/c8/Hero_Nimbus_%28HotA%29.png',
                type: 'Necromancer',
                specialty: 'Eagle_Eye',
                skills: ['Basic_Necromancy', 'Basic_Eagle_Eye']
            },
            Sandro: {
                img: 'https://heroes.thelazy.net/images/1/1a/Hero_Sandro.png',
                type: 'Necromancer',
                specialty: 'Sorcery',
                skills: ['Basic_Necromancy', 'Basic_Sorcery']
            },
            Septienna: {
                img: 'https://heroes.thelazy.net/images/7/72/Hero_Septienna.png',
                type: 'Necromancer',
                specialty: 'Death_Ripple',
                skills: ['Basic_Necromancy', 'Basic_Scholar']
            },
            Thant: {
                img: 'https://heroes.thelazy.net/images/a/a1/Hero_Thant.png',
                type: 'Necromancer',
                specialty: 'Animate_Dead',
                skills: ['Basic_Necromancy', 'Basic_Mysticism']
            },
            Vidomina: {
                img: 'https://heroes.thelazy.net/images/6/6a/Hero_Vidomina.png',
                type: 'Necromancer',
                specialty: 'Necromancy',
                skills: ['Advanced_Necromancy']
            },
            Xsi: {
                img: 'https://heroes.thelazy.net/images/8/85/Hero_Xsi.png',
                type: 'Necromancer',
                specialty: 'Stone_Skin',
                skills: ['Basic_Necromancy', 'Basic_Learning']
            }
        }
    },
    Dungeon: {
        img: 'https://heroes.thelazy.net/images/b/b0/Town_portrait_Dungeon_small.gif',
        heroes: {
            Ajit: {
                img: 'https://heroes.thelazy.net/images/c/c9/Hero_Ajit_%28HotA%29.png',
                type: 'Overlord',
                specialty: 'Beholders',
                skills: ['Basic_Leadership', 'Basic_Interference']
            },
            Arlach: {
                img: 'https://heroes.thelazy.net/images/a/ae/Hero_Arlach.png',
                type: 'Overlord',
                specialty: 'Ballista',
                skills: ['Basic_Offense', 'Basic_Artillery']
            },
            Dace: {
                img: 'https://heroes.thelazy.net/images/a/aa/Hero_Dace.png',
                type: 'Overlord',
                specialty: 'Minotaurs',
                skills: ['Basic_Tactics', 'Basic_Offense']
            },
            Damacon: {
                img: 'https://heroes.thelazy.net/images/5/5b/Hero_Damacon.png',
                type: 'Overlord',
                specialty: 'Gold',
                skills: ['Advanced_Offense']
            },
            Gunnar: {
                img: 'https://heroes.thelazy.net/images/a/af/Hero_Gunnar_%28HotA%29.png',
                type: 'Overlord',
                specialty: 'Logistics',
                skills: ['Basic_Tactics', 'Basic_Logistics']
            },
            Lorelei: {
                img: 'https://heroes.thelazy.net/images/e/ea/Hero_Lorelei.png',
                type: 'Overlord',
                specialty: 'Harpies',
                skills: ['Basic_Leadership', 'Basic_Scouting']
            },
            Shakti: {
                img: 'https://heroes.thelazy.net/images/1/18/Hero_Shakti.png',
                type: 'Overlord',
                specialty: 'Troglodytes',
                skills: ['Basic_Offense', 'Basic_Tactics']
            },
            Synca: {
                img: 'https://heroes.thelazy.net/images/9/93/Hero_Synca.png',
                type: 'Overlord',
                specialty: 'Manticores',
                skills: ['Basic_Leadership', 'Basic_Scholar']
            },
            Mutare: {
                img: 'https://heroes.thelazy.net/images/6/68/Hero_Mutare.png',
                type: 'Overlord',
                specialty: 'Dragons',
                skills: ['Basic_Estates', 'Basic_Tactics']
            },
            Mutare_Drake: {
                img: 'https://heroes.thelazy.net/images/e/e4/Hero_Mutare_Drake.png',
                type: 'Overlord',
                specialty: 'Dragons',
                skills: ['Basic_Estates', 'Basic_Tactics']
            },
            Alamar: {
                img: 'https://heroes.thelazy.net/images/5/5c/Hero_Alamar_%28HotA%29.png',
                type: 'Warlock',
                specialty: 'Resurrection',
                skills: ['Basic_Wisdom', 'Basic_Scholar']
            },
            Darkstorn: {
                img: 'https://heroes.thelazy.net/images/8/87/Hero_Darkstorn.png',
                type: 'Warlock',
                specialty: 'Stone_Skin',
                skills: ['Basic_Wisdom', 'Basic_Learning']
            },
            Deemer: {
                img: 'https://heroes.thelazy.net/images/f/fe/Hero_Deemer.png',
                type: 'Warlock',
                specialty: 'Meteor_Shower',
                skills: ['Basic_Wisdom', 'Basic_Scouting']
            },
            Geon: {
                img: 'https://heroes.thelazy.net/images/4/48/Hero_Geon.png',
                type: 'Warlock',
                specialty: 'Eagle_Eye',
                skills: ['Basic_Wisdom', 'Basic_Eagle_Eye']
            },
            Jaegar: {
                img: 'https://heroes.thelazy.net/images/9/93/Hero_Jaegar.png',
                type: 'Warlock',
                specialty: 'Mysticism',
                skills: ['Basic_Wisdom', 'Basic_Mysticism']
            },
            Jeddite: {
                img: 'https://heroes.thelazy.net/images/c/c9/Hero_Jeddite.png',
                type: 'Warlock',
                specialty: 'Resurrection',
                skills: ['Advanced_Wisdom']
            },
            Malekith: {
                img: 'https://heroes.thelazy.net/images/5/5d/Hero_Malekith.png',
                type: 'Warlock',
                specialty: 'Sorcery',
                skills: ['Basic_Wisdom', 'Basic_Sorcery']
            },
            Sephinroth: {
                img: 'https://heroes.thelazy.net/images/1/1e/Hero_Sephinroth_%28HotA%29.png',
                type: 'Warlock',
                specialty: 'Crystal',
                skills: ['Basic_Wisdom', 'Basic_Intelligence']
            }
        }
    },
    Stronghold: {
        img: 'https://heroes.thelazy.net/images/a/a4/Town_portrait_Stronghold_small.gif',
        heroes: {
            Crag_Hack: {
                img: 'https://heroes.thelazy.net/images/7/7e/Hero_Crag_Hack.png',
                type: 'Barbarian',
                specialty: 'Offense',
                skills: ['Advanced_Offense']
            },
            Gretchin: {
                img: 'https://heroes.thelazy.net/images/2/22/Hero_Gretchin_%28HotA%29.png',
                type: 'Barbarian',
                specialty: 'Goblins',
                skills: ['Basic_Offense', 'Basic_Pathfinding']
            },
            Gurnisson: {
                img: 'https://heroes.thelazy.net/images/5/5d/Hero_Gurnisson_%28HotA%29.png',
                type: 'Barbarian',
                specialty: 'Ballista',
                skills: ['Basic_Offense', 'Basic_Artillery']
            },
            Jabarkas: {
                img: 'https://heroes.thelazy.net/images/f/fb/Hero_Jabarkas.png',
                type: 'Barbarian',
                specialty: 'Orcs',
                skills: ['Basic_Offense', 'Basic_Archery']
            },
            Krellion: {
                img: 'https://heroes.thelazy.net/images/f/f9/Hero_Krellion.png',
                type: 'Barbarian',
                specialty: 'Ogres',
                skills: ['Basic_Offense', 'Basic_Interference']
            },
            Shiva: {
                img: 'https://heroes.thelazy.net/images/5/5d/Hero_Shiva.png',
                type: 'Barbarian',
                specialty: 'Rocs',
                skills: ['Basic_Offense', 'Basic_Scouting']
            },
            Tyraxor: {
                img: 'https://heroes.thelazy.net/images/b/b6/Hero_Tyraxor_%28HotA%29.png',
                type: 'Barbarian',
                specialty: 'Wolf_Riders',
                skills: ['Basic_Offense', 'Basic_Tactics']
            },
            Yog: {
                img: 'https://heroes.thelazy.net/images/3/39/Hero_Yog.png',
                type: 'Barbarian',
                specialty: 'Cyclops',
                skills: ['Basic_Offense', 'Basic_Ballistics']
            },
            Boragus: {
                img: 'https://heroes.thelazy.net/images/7/72/Hero_Boragus.png',
                type: 'Barbarian',
                specialty: 'Ogres',
                skills: ['Basic_Offense', 'Basic_Tactics']
            },
            Kilgor: {
                img: 'https://heroes.thelazy.net/images/2/2b/Hero_Kilgor.png',
                type: 'Barbarian',
                specialty: 'Behemoths',
                skills: ['Advanced_Offense']
            },
            Dessa: {
                img: 'https://heroes.thelazy.net/images/c/c9/Hero_Dessa.png',
                type: 'Battle_Mage',
                specialty: 'Logistics',
                skills: ['Basic_Wisdom', 'Basic_Logistics']
            },
            Gird: {
                img: 'https://heroes.thelazy.net/images/8/8b/Hero_Gird.png',
                type: 'Battle_Mage',
                specialty: 'Sorcery',
                skills: ['Basic_Wisdom', 'Basic_Sorcery']
            },
            Gundula: {
                img: 'https://heroes.thelazy.net/images/e/ea/Hero_Gundula.png',
                type: 'Battle_Mage',
                specialty: 'Offense',
                skills: ['Basic_Wisdom', 'Basic_Offense']
            },
            Oris: {
                img: 'https://heroes.thelazy.net/images/3/3a/Hero_Oris.png',
                type: 'Battle_Mage',
                specialty: 'Eagle_Eye',
                skills: ['Basic_Wisdom', 'Basic_Eagle_Eye']
            },
            Saurug: {
                img: 'https://heroes.thelazy.net/images/d/d7/Hero_Saurug.png',
                type: 'Battle_Mage',
                specialty: 'Gems',
                skills: ['Basic_Wisdom', 'Basic_Interference']
            },
            Terek: {
                img: 'https://heroes.thelazy.net/images/3/3f/Hero_Terek.png',
                type: 'Battle_Mage',
                specialty: 'Haste',
                skills: ['Basic_Wisdom', 'Basic_Tactics']
            },
            Vey: {
                img: 'https://heroes.thelazy.net/images/0/06/Hero_Vey_%28HotA%29.png',
                type: 'Battle_Mage',
                specialty: 'Ogres',
                skills: ['Basic_Wisdom', 'Basic_Leadership']
            },
            Zubin: {
                img: 'https://heroes.thelazy.net/images/1/1b/Hero_Zubin_%28HotA%29.png',
                type: 'Battle_Mage',
                specialty: 'Precision',
                skills: ['Basic_Wisdom', 'Basic_Artillery']
            }
        }
    },
    Fortress: {
        img: 'https://heroes.thelazy.net/images/f/f6/Town_portrait_Fortress_small.gif',
        heroes: {
            Alkin: {
                img: 'https://heroes.thelazy.net/images/a/ad/Hero_Alkin.png',
                type: 'Beastmaster',
                specialty: 'Gorgons',
                skills: ['Basic_Armorer', 'Basic_Offense']
            },
            Broghild: {
                img: 'https://heroes.thelazy.net/images/f/fd/Hero_Broghild.png',
                type: 'Beastmaster',
                specialty: 'Wyverns',
                skills: ['Basic_Armorer', 'Basic_Scouting']
            },
            Bron: {
                img: 'https://heroes.thelazy.net/images/8/82/Hero_Bron.png',
                type: 'Beastmaster',
                specialty: 'Basilisks',
                skills: ['Basic_Armorer', 'Basic_Interference']
            },
            Drakon: {
                img: 'https://heroes.thelazy.net/images/7/74/Hero_Drakon.png',
                type: 'Beastmaster',
                specialty: 'Gnolls',
                skills: ['Basic_Armorer', 'Basic_Leadership']
            },
            Gerwulf: {
                img: 'https://heroes.thelazy.net/images/3/39/Hero_Gerwulf.png',
                type: 'Beastmaster',
                specialty: 'Ballista',
                skills: ['Basic_Armorer', 'Basic_Artillery']
            },
            Korbac: {
                img: 'https://heroes.thelazy.net/images/6/6b/Hero_Korbac.png',
                type: 'Beastmaster',
                specialty: 'Serpent_Flies',
                skills: ['Basic_Armorer', 'Basic_Pathfinding']
            },
            Tazar: {
                img: 'https://heroes.thelazy.net/images/d/dc/Hero_Tazar.png',
                type: 'Beastmaster',
                specialty: 'Armorer',
                skills: ['Advanced_Armorer']
            },
            Wystan: {
                img: 'https://heroes.thelazy.net/images/3/33/Hero_Wystan.png',
                type: 'Beastmaster',
                specialty: 'Lizardmen',
                skills: ['Basic_Armorer', 'Basic_Archery']
            },
            Andra: {
                img: 'https://heroes.thelazy.net/images/6/6c/Hero_Andra.png',
                type: 'Witch',
                specialty: 'Intelligence',
                skills: ['Basic_Wisdom', 'Basic_Intelligence']
            },
            Merist: {
                img: 'https://heroes.thelazy.net/images/a/a6/Hero_Merist_%28HotA%29.png',
                type: 'Witch',
                specialty: 'Stone_Skin',
                skills: ['Basic_Wisdom', 'Basic_Learning']
            },
            Mirlanda: {
                img: 'https://heroes.thelazy.net/images/f/fe/Hero_Mirlanda.png',
                type: 'Witch',
                specialty: 'Weakness',
                skills: ['Advanced_Wisdom']
            },
            Rosic: {
                img: 'https://heroes.thelazy.net/images/6/6f/Hero_Rosic.png',
                type: 'Witch',
                specialty: 'Mysticism',
                skills: ['Basic_Wisdom', 'Basic_Mysticism']
            },
            Styg: {
                img: 'https://heroes.thelazy.net/images/7/7e/Hero_Styg.png',
                type: 'Witch',
                specialty: 'Sorcery',
                skills: ['Basic_Wisdom', 'Basic_Sorcery']
            },
            Tiva: {
                img: 'https://heroes.thelazy.net/images/3/3d/Hero_Tiva_%28HotA%29.png',
                type: 'Witch',
                specialty: 'Eagle_Eye',
                skills: ['Basic_Wisdom', 'Basic_Eagle_Eye']
            },
            Verdish: {
                img: 'https://heroes.thelazy.net/images/1/1a/Hero_Verdish.png',
                type: 'Witch',
                specialty: 'First_Aid',
                skills: ['Basic_Wisdom', 'Basic_First_Aid']
            },
            Voy: {
                img: 'https://heroes.thelazy.net/images/b/ba/Hero_Voy.png',
                type: 'Witch',
                specialty: 'Navigation',
                skills: ['Basic_Wisdom', 'Basic_Navigation']
            },
            Adrienne: {
                img: 'https://heroes.thelazy.net/images/1/1d/Hero_Adrienne.png',
                type: 'Witch',
                specialty: 'Fire_Magic',
                skills: ['Basic_Wisdom', 'Expert_Fire_Magic']
            },
            Kinkeria: {
                img: 'https://heroes.thelazy.net/images/0/0f/Hero_Kinkeria.png',
                type: 'Witch',
                specialty: 'Learning',
                skills: ['Basic_Wisdom', 'Basic_Learning']
            }
        }
    },
    Conflux: {
        img: 'https://heroes.thelazy.net/images/4/48/Town_portrait_Conflux_small.gif',
        heroes: {
            Erdamon: {
                img: 'https://heroes.thelazy.net/images/3/3b/Hero_Erdamon_%28HotA%29.png',
                type: 'Planeswalker',
                specialty: 'Earth_Elementals',
                skills: ['Basic_Tactics', 'Basic_Estates']
            },
            Fiur: {
                img: 'https://heroes.thelazy.net/images/0/0e/Hero_Fiur.png',
                type: 'Planeswalker',
                specialty: 'Fire_Elementals',
                skills: ['Advanced_Offense']
            },
            Ignissa: {
                img: 'https://heroes.thelazy.net/images/a/a3/Hero_Ignissa.png',
                type: 'Planeswalker',
                specialty: 'Fire_Elementals',
                skills: ['Basic_Offense', 'Basic_Artillery']
            },
            Kalt: {
                img: 'https://heroes.thelazy.net/images/c/c9/Hero_Kalt_%28HotA%29.png',
                type: 'Planeswalker',
                specialty: 'Water_Elementals',
                skills: ['Basic_Tactics', 'Basic_Learning']
            },
            Lacus: {
                img: 'https://heroes.thelazy.net/images/9/9f/Hero_Lacus.png',
                type: 'Planeswalker',
                specialty: 'Water_Elementals',
                skills: ['Advanced_Tactics']
            },
            Monere: {
                img: 'https://heroes.thelazy.net/images/f/fd/Hero_Monere_%28HotA%29.png',
                type: 'Planeswalker',
                specialty: 'Psychic_Elementals',
                skills: ['Basic_Offense', 'Basic_Logistics']
            },
            Pasis: {
                img: 'https://heroes.thelazy.net/images/2/2d/Hero_Pasis_%28HotA%29.png',
                type: 'Planeswalker',
                specialty: 'Psychic_Elementals',
                skills: ['Basic_Offense', 'Basic_Artillery']
            },
            Thunar: {
                img: 'https://heroes.thelazy.net/images/2/22/Hero_Thunar.png',
                type: 'Planeswalker',
                specialty: 'Earth_Elementals',
                skills: ['Basic_Tactics', 'Basic_Estates']
            },
            Aenain: {
                img: 'https://heroes.thelazy.net/images/2/20/Hero_Aenain_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Disrupting_Ray',
                skills: ['Basic_Wisdom', 'Basic_Air_Magic']
            },
            Brissa: {
                img: 'https://heroes.thelazy.net/images/c/c4/Hero_Brissa_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Haste',
                skills: ['Basic_Wisdom', 'Basic_Air_Magic']
            },
            Ciele: {
                img: 'https://heroes.thelazy.net/images/a/aa/Hero_Ciele_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Magic_Arrow',
                skills: ['Basic_Wisdom', 'Basic_Water_Magic']
            },
            Gelare: {
                img: 'https://heroes.thelazy.net/images/d/dd/Hero_Gelare_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Gold',
                skills: ['Basic_Wisdom', 'Basic_Water_Magic']
            },
            Grindan: {
                img: 'https://heroes.thelazy.net/images/8/8d/Hero_Grindan_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Gold',
                skills: ['Basic_Wisdom', 'Basic_Earth_Magic']
            },
            Inteus: {
                img: 'https://heroes.thelazy.net/images/9/90/Hero_Inteus_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Bloodlust',
                skills: ['Basic_Wisdom', 'Basic_Fire_Magic']
            },
            Labetha: {
                img: 'https://heroes.thelazy.net/images/9/90/Hero_Labetha_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Stone_Skin',
                skills: ['Basic_Wisdom', 'Basic_Earth_Magic']
            },
            Luna: {
                img: 'https://heroes.thelazy.net/images/3/34/Hero_Luna_%28HotA%29.png',
                type: 'Elementalist',
                specialty: 'Fire_Wall',
                skills: ['Basic_Wisdom', 'Basic_Fire_Magic']
            }
        }
    },
    Cove: {
        img: 'https://heroes.thelazy.net/images/4/4a/Town_portrait_Cove_small.gif',
        heroes: {
            Anabel: {
                img: 'https://heroes.thelazy.net/images/1/1d/Hero_Anabel.png',
                type: 'Captain',
                specialty: 'Pirates',
                skills: ['Basic_Offense', 'Basic_Archery']
            },
            Cassiopeia: {
                img: 'https://heroes.thelazy.net/images/d/d9/Hero_Cassiopeia.png',
                type: 'Captain',
                specialty: 'Nymphs',
                skills: ['Basic_Offense', 'Basic_Tactics']
            },
            Corkes: {
                img: 'https://heroes.thelazy.net/images/c/c5/Hero_Corkes.png',
                type: 'Captain',
                specialty: 'Offense',
                skills: ['Basic_Offense', 'Basic_Pathfinding']
            },
            Derek: {
                img: 'https://heroes.thelazy.net/images/b/bc/Hero_Derek.png',
                type: 'Captain',
                specialty: 'Crew_Mates',
                skills: ['Basic_Offense', 'Basic_Leadership']
            },
            Elmore: {
                img: 'https://heroes.thelazy.net/images/9/9f/Hero_Elmore.png',
                type: 'Captain',
                specialty: 'Navigation',
                skills: ['Advanced_Navigation']
            },
            Illor: {
                img: 'https://heroes.thelazy.net/images/e/e9/Hero_Illor.png',
                type: 'Captain',
                specialty: 'Stormbirds',
                skills: ['Basic_Armorer', 'Basic_Tactics']
            },
            Jeremy: {
                img: 'https://heroes.thelazy.net/images/9/9d/Hero_Jeremy.png',
                type: 'Captain',
                specialty: 'Cannon',
                skills: ['Basic_Offense', 'Basic_Artillery']
            },
            Leena: {
                img: 'https://heroes.thelazy.net/images/0/0b/Hero_Leena.png',
                type: 'Captain',
                specialty: 'Gold',
                skills: ['Basic_Pathfinding', 'Basic_Estates']
            },
            Miriam: {
                img: 'https://heroes.thelazy.net/images/1/14/Hero_Miriam.png',
                type: 'Captain',
                specialty: 'Scouting',
                skills: ['Basic_Logistics', 'Basic_Scouting']
            },
            Bidley: {
                img: 'https://heroes.thelazy.net/images/e/e8/Hero_Bidley.png',
                type: 'Captain',
                specialty: 'Sea_Dogs',
                skills: ['Advanced_Offense']
            },
            Tark: {
                img: 'https://heroes.thelazy.net/images/6/66/Hero_Tark.png',
                type: 'Captain',
                specialty: 'Nix',
                skills: ['Basic_Offense', 'Basic_Armorer']
            },
            Andal: {
                img: 'https://heroes.thelazy.net/images/b/b3/Hero_Andal.png',
                type: 'Navigator',
                specialty: 'Crystal',
                skills: ['Basic_Wisdom', 'Basic_Pathfinding']
            },
            Astra: {
                img: 'https://heroes.thelazy.net/images/d/dc/Hero_Astra.png',
                type: 'Navigator',
                specialty: 'Cure',
                skills: ['Basic_Wisdom', 'Basic_Luck']
            },
            Casmetra: {
                img: 'https://heroes.thelazy.net/images/0/09/Hero_Casmetra.png',
                type: 'Navigator',
                specialty: 'Sea_Witches',
                skills: ['Basic_Wisdom', 'Basic_Water_Magic']
            },
            Dargem: {
                img: 'https://heroes.thelazy.net/images/f/f0/Hero_Dargem.png',
                type: 'Navigator',
                specialty: 'Air_Shield',
                skills: ['Basic_Wisdom', 'Basic_Tactics']
            },
            Eovacius: {
                img: 'https://heroes.thelazy.net/images/3/3b/Hero_Eovacius.png',
                type: 'Navigator',
                specialty: 'Clone',
                skills: ['Basic_Wisdom', 'Basic_Intelligence']
            },
            Manfred: {
                img: 'https://heroes.thelazy.net/images/e/e4/Hero_Manfred.png',
                type: 'Navigator',
                specialty: 'Fireball',
                skills: ['Basic_Wisdom', 'Basic_Fire_Magic']
            },
            Spint: {
                img: 'https://heroes.thelazy.net/images/1/1a/Hero_Spint.png',
                type: 'Navigator',
                specialty: 'Sorcery',
                skills: ['Basic_Wisdom', 'Basic_Sorcery']
            },
            Zilare: {
                img: 'https://heroes.thelazy.net/images/9/91/Hero_Zilare.png',
                type: 'Navigator',
                specialty: 'Forgetfulness',
                skills: ['Basic_Wisdom', 'Basic_Interference']
            }
        }
    }
};

const SKILLS = {
    Basic_Offense: {img: 'https://heroes.thelazy.net/images/c/c1/Basic_Offense.png'},
    Basic_Archery: {img: 'https://heroes.thelazy.net/images/6/6b/Basic_Archery.png'},
    Basic_Tactics: {img: 'https://heroes.thelazy.net/images/8/81/Basic_Tactics.png'},
    Basic_Pathfinding: {img: 'https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png'},
    Basic_Leadership: {img: 'https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png'},
    Advanced_Navigation: {img: 'https://heroes.thelazy.net/images/b/bf/Advanced_Navigation.png'},
    Basic_Armorer: {img: 'https://heroes.thelazy.net/images/9/99/Basic_Armorer.png'},
    Basic_Artillery: {img: 'https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png'},
    Basic_Estates: {img: 'https://heroes.thelazy.net/images/3/3a/Basic_Estates.png'},
    Basic_Logistics: {img: 'https://heroes.thelazy.net/images/7/75/Basic_Logistics.png'},
    Basic_Scouting: {img: 'https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png'},
    Advanced_Offense: {img: 'https://heroes.thelazy.net/images/2/22/Advanced_Offense.png'},
    Basic_Wisdom: {img: 'https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png'},
    Basic_Luck: {img: 'https://heroes.thelazy.net/images/f/fc/Basic_Luck.png'},
    Basic_Water_Magic: {img: 'https://heroes.thelazy.net/images/5/59/Basic_Water_Magic.png'},
    Basic_Intelligence: {img: 'https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png'},
    Basic_Fire_Magic: {img: 'https://heroes.thelazy.net/images/0/00/Basic_Fire_Magic.png'},
    Basic_Sorcery: {img: 'https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png'},
    Basic_Interference: {img: 'https://heroes.thelazy.net/images/6/60/Basic_Interference.png'},
    Basic_Navigation: {img: 'https://heroes.thelazy.net/images/f/f9/Basic_Navigation.png'},
    Advanced_Leadership: {img: 'https://heroes.thelazy.net/images/f/f8/Advanced_Leadership.png'},
    Basic_Diplomacy: {img: 'https://heroes.thelazy.net/images/3/38/Basic_Diplomacy.png'},
    Advanced_Wisdom: {img: 'https://heroes.thelazy.net/images/5/52/Advanced_Wisdom.png'},
    Basic_Mysticism: {img: 'https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png'},
    Basic_Learning: {img: 'https://heroes.thelazy.net/images/a/a4/Basic_Learning.png'},
    Basic_First_Aid: {img: 'https://heroes.thelazy.net/images/3/30/Basic_First_Aid.png'},
    Basic_Eagle_Eye: {img: 'https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png'},
    Advanced_Archery: {img: 'https://heroes.thelazy.net/images/2/25/Advanced_Archery.png'},
    Advanced_Resistance: {img: 'https://heroes.thelazy.net/images/f/f1/Advanced_Resistance.png'},
    Advanced_Interference: {img: 'https://heroes.thelazy.net/images/c/c4/Advanced_Interference.png'},
    Basic_Scholar: {img: 'https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png'},
    Basic_Ballistics: {img: 'https://heroes.thelazy.net/images/d/da/Basic_Ballistics.png'},
    Advanced_Scholar: {img: 'https://heroes.thelazy.net/images/2/24/Advanced_Scholar.png'},
    Advanced_Scouting: {img: 'https://heroes.thelazy.net/images/0/0e/Advanced_Scouting.png'},
    Advanced_Armorer: {img: 'https://heroes.thelazy.net/images/0/06/Advanced_Armorer.png'},
    Basic_Necromancy: {img: 'https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png'},
    Advanced_Necromancy: {img: 'https://heroes.thelazy.net/images/7/7f/Advanced_Necromancy.png'},
    Expert_Fire_Magic: {img: 'https://heroes.thelazy.net/images/8/85/Expert_Fire_Magic.png'},
    Advanced_Tactics: {img: 'https://heroes.thelazy.net/images/0/03/Advanced_Tactics.png'},
    Basic_Air_Magic: {img: 'https://heroes.thelazy.net/images/4/44/Basic_Air_Magic.png'},
    Basic_Earth_Magic: {img: 'https://heroes.thelazy.net/images/4/48/Basic_Earth_Magic.png'}
};

const SPECILALITIES = {
    Pirates: {img: 'https://heroes.thelazy.net/images/1/11/Specialty_Pirates.png'},
    Nymphs: {img: 'https://heroes.thelazy.net/images/e/e1/Specialty_Nymphs.png'},
    Offense: {img: 'https://heroes.thelazy.net/images/b/b9/Specialty_Offense.png'},
    Crew_Mates: {img: 'https://heroes.thelazy.net/images/b/b6/Specialty_Crew_Mates.png'},
    Navigation: {img: 'https://heroes.thelazy.net/images/c/cc/Specialty_Navigation.png'},
    Stormbirds: {img: 'https://heroes.thelazy.net/images/d/d4/Specialty_Stormbirds.png'},
    Cannon: {img: 'https://heroes.thelazy.net/images/f/f8/Specialty_Cannon.png'},
    Gold: {img: 'https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png'},
    Scouting: {img: 'https://heroes.thelazy.net/images/e/e3/Specialty_Scouting.png'},
    Sea_Dogs: {img: 'https://heroes.thelazy.net/images/3/3a/Specialty_Sea_Dogs.png'},
    Nix: {img: 'https://heroes.thelazy.net/images/8/8f/Specialty_Nix.png'},
    Crystal: {img: 'https://heroes.thelazy.net/images/6/6c/Specialty_Crystal.png'},
    Cure: {img: 'https://heroes.thelazy.net/images/7/7b/Cure.png'},
    Sea_Witches: {img: 'https://heroes.thelazy.net/images/c/c2/Specialty_Sea_Witches.png'},
    Air_Shield: {img: 'https://heroes.thelazy.net/images/d/d8/Air_Shield.png'},
    Clone: {img: 'https://heroes.thelazy.net/images/c/c9/Specialty_Clone.png'},
    Fireball: {img: 'https://heroes.thelazy.net/images/a/ae/Fireball.png'},
    Sorcery: {img: 'https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png'},
    Forgetfulness: {img: 'https://heroes.thelazy.net/images/f/f8/Forgetfulness.png'},
    Ballista: {img: 'https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png'},
    Griffins: {img: 'https://heroes.thelazy.net/images/e/e8/Specialty_Griffins.png'},
    Archery: {img: 'https://heroes.thelazy.net/images/0/0e/Specialty_Archery.png'},
    Swordsmen: {img: 'https://heroes.thelazy.net/images/9/90/Specialty_Swordsmen.png'},
    Archers: {img: 'https://heroes.thelazy.net/images/6/67/Specialty_Archers.png'},
    Cavaliers: {img: 'https://heroes.thelazy.net/images/6/64/Specialty_Cavaliers.png'},
    Estates: {img: 'https://heroes.thelazy.net/images/8/8f/Specialty_Estates.png'},
    Speed: {img: 'https://heroes.thelazy.net/images/c/ca/Specialty_Speed.png'},
    Bless: {img: 'https://heroes.thelazy.net/images/5/51/Bless.png'},
    Frost_Ring: {img: 'https://heroes.thelazy.net/images/9/99/Specialty_Frost_Ring.png'},
    Weakness: {img: 'https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png'},
    Monks: {img: 'https://heroes.thelazy.net/images/f/fc/Specialty_Monks.png'},
    Prayer: {img: 'https://heroes.thelazy.net/images/f/f9/Specialty_Prayer.png'},
    First_Aid: {img: 'https://heroes.thelazy.net/images/7/70/Specialty_First_Aid.png'},
    Eagle_Eye: {img: 'https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png'},
    Unicorns: {img: 'https://heroes.thelazy.net/images/2/2e/Specialty_Unicorns.png'},
    Elves: {img: 'https://heroes.thelazy.net/images/f/f1/Specialty_Elves.png'},
    Logistics: {img: 'https://heroes.thelazy.net/images/8/85/Specialty_Logistics.png'},
    Armorer: {img: 'https://heroes.thelazy.net/images/3/36/Specialty_Armorer.png'},
    Dendroids: {img: 'https://heroes.thelazy.net/images/0/0a/Specialty_Dendroids.png'},
    Resistance: {img: 'https://heroes.thelazy.net/images/6/68/Specialty_Resistance.png'},
    Dwarves: {img: 'https://heroes.thelazy.net/images/a/ab/Specialty_Dwarves.png'},
    Sharpshooters: {img: 'https://heroes.thelazy.net/images/f/fe/Specialty_Sharpshooters.png'},
    Interference: {img: 'https://heroes.thelazy.net/images/2/2c/Specialty_Interference.png'},
    Pegasi: {img: 'https://heroes.thelazy.net/images/5/51/Specialty_Pegasi.png'},
    Ice_Bolt: {img: 'https://heroes.thelazy.net/images/9/97/Ice_Bolt.png'},
    Slayer: {img: 'https://heroes.thelazy.net/images/f/f2/Specialty_Slayer.png'},
    Intelligence: {img: 'https://heroes.thelazy.net/images/7/74/Specialty_Intelligence.png'},
    Fortune: {img: 'https://heroes.thelazy.net/images/b/bf/Fortune.png'},
    Nagas: {img: 'https://heroes.thelazy.net/images/2/2c/Specialty_Nagas.png'},
    Genies: {img: 'https://heroes.thelazy.net/images/0/0c/Specialty_Genies.png'},
    Golems: {img: 'https://heroes.thelazy.net/images/d/de/Specialty_Golems.png'},
    Gargoyles: {img: 'https://heroes.thelazy.net/images/c/c3/Specialty_Gargoyles.png'},
    Mercury: {img: 'https://heroes.thelazy.net/images/2/2a/Specialty_Mercury.png'},
    Hypnotize: {img: 'https://heroes.thelazy.net/images/7/77/Specialty_Hypnotize.png'},
    Haste: {img: 'https://heroes.thelazy.net/images/3/35/Haste.png'},
    Mysticism: {img: 'https://heroes.thelazy.net/images/6/6a/Specialty_Mysticism.png'},
    Chain_Lightning: {img: 'https://heroes.thelazy.net/images/e/ec/Specialty_Chain_Lightning.png'},
    Magi: {img: 'https://heroes.thelazy.net/images/2/2e/Specialty_Magi.png'},
    Enchanters: {img: 'https://heroes.thelazy.net/images/e/e5/Specialty_Enchanters.png'},
    Gogs: {img: 'https://heroes.thelazy.net/images/7/7e/Specialty_Gogs.png'},
    Hell_Hounds: {img: 'https://heroes.thelazy.net/images/4/4e/Specialty_Hell_Hounds.png'},
    Imps: {img: 'https://heroes.thelazy.net/images/5/59/Specialty_Imps.png'},
    Demons: {img: 'https://heroes.thelazy.net/images/2/25/Specialty_Demons.png'},
    Pit_Fiends: {img: 'https://heroes.thelazy.net/images/e/ee/Specialty_Pit_Fiends.png'},
    Efreet: {img: 'https://heroes.thelazy.net/images/2/29/Specialty_Efreet.png'},
    Devils: {img: 'https://heroes.thelazy.net/images/c/c4/Specialty_Devils.png'},
    Bloodlust: {img: 'https://heroes.thelazy.net/images/1/11/Bloodlust.png'},
    Sulfur: {img: 'https://heroes.thelazy.net/images/8/87/Specialty_Sulfur.png'},
    'Inferno_(spell)': {img: 'https://heroes.thelazy.net/images/0/0e/Specialty_Inferno_%28spell%29.png'},
    Wights: {img: 'https://heroes.thelazy.net/images/2/25/Specialty_Wights.png'},
    Skeletons: {img: 'https://heroes.thelazy.net/images/d/d2/Specialty_Skeletons.png'},
    Necromancy: {img: 'https://heroes.thelazy.net/images/6/6f/Specialty_Necromancy.png'},
    Liches: {img: 'https://heroes.thelazy.net/images/c/cb/Specialty_Liches.png'},
    Walking_Dead: {img: 'https://heroes.thelazy.net/images/0/00/Specialty_Walking_Dead.png'},
    Black_Knights: {img: 'https://heroes.thelazy.net/images/c/c5/Specialty_Black_Knights.png'},
    Vampires: {img: 'https://heroes.thelazy.net/images/a/aa/Specialty_Vampires.png'},
    Meteor_Shower: {img: 'https://heroes.thelazy.net/images/6/6b/Specialty_Meteor_Shower.png'},
    Death_Ripple: {img: 'https://heroes.thelazy.net/images/3/3f/Death_Ripple.png'},
    Animate_Dead: {img: 'https://heroes.thelazy.net/images/f/f8/Animate_Dead.png'},
    Stone_Skin: {img: 'https://heroes.thelazy.net/images/2/2d/Stone_Skin.png'},
    Beholders: {img: 'https://heroes.thelazy.net/images/3/31/Specialty_Beholders.png'},
    Minotaurs: {img: 'https://heroes.thelazy.net/images/9/96/Specialty_Minotaurs.png'},
    Harpies: {img: 'https://heroes.thelazy.net/images/a/a2/Specialty_Harpies.png'},
    Troglodytes: {img: 'https://heroes.thelazy.net/images/8/88/Specialty_Troglodytes.png'},
    Manticores: {img: 'https://heroes.thelazy.net/images/3/3b/Specialty_Manticores.png'},
    Dragons: {img: 'https://heroes.thelazy.net/images/7/75/Specialty_Dragons.png'},
    Resurrection: {img: 'https://heroes.thelazy.net/images/b/b0/Specialty_Resurrection.png'},
    Goblins: {img: 'https://heroes.thelazy.net/images/c/c9/Specialty_Goblins.png'},
    Orcs: {img: 'https://heroes.thelazy.net/images/1/14/Specialty_Orcs.png'},
    Ogres: {img: 'https://heroes.thelazy.net/images/9/92/Specialty_Ogres.png'},
    Rocs: {img: 'https://heroes.thelazy.net/images/4/42/Specialty_Rocs.png'},
    Wolf_Riders: {img: 'https://heroes.thelazy.net/images/4/48/Specialty_Wolf_Riders.png'},
    Cyclops: {img: 'https://heroes.thelazy.net/images/d/d5/Specialty_Cyclops.png'},
    Behemoths: {img: 'https://heroes.thelazy.net/images/3/33/Specialty_Behemoths.png'},
    Gems: {img: 'https://heroes.thelazy.net/images/4/41/Specialty_Gems.png'},
    Precision: {img: 'https://heroes.thelazy.net/images/a/af/Specialty_Precision.png'},
    Gorgons: {img: 'https://heroes.thelazy.net/images/c/c1/Specialty_Gorgons.png'},
    Wyverns: {img: 'https://heroes.thelazy.net/images/d/d5/Specialty_Wyverns.png'},
    Basilisks: {img: 'https://heroes.thelazy.net/images/d/d0/Specialty_Basilisks.png'},
    Gnolls: {img: 'https://heroes.thelazy.net/images/9/9e/Specialty_Gnolls.png'},
    Serpent_Flies: {img: 'https://heroes.thelazy.net/images/6/66/Specialty_Serpent_Flies.png'},
    Lizardmen: {img: 'https://heroes.thelazy.net/images/0/07/Specialty_Lizardmen.png'},
    Fire_Magic: {img: 'https://heroes.thelazy.net/images/c/ce/Specialty_Fire_Magic.png'},
    Learning: {img: 'https://heroes.thelazy.net/images/c/c6/Specialty_Learning.png'},
    Earth_Elementals: {img: 'https://heroes.thelazy.net/images/f/fa/Specialty_Earth_Elementals.png'},
    Fire_Elementals: {img: 'https://heroes.thelazy.net/images/8/8e/Specialty_Fire_Elementals.png'},
    Water_Elementals: {img: 'https://heroes.thelazy.net/images/e/e5/Specialty_Water_Elementals.png'},
    Psychic_Elementals: {img: 'https://heroes.thelazy.net/images/0/0c/Specialty_Psychic_Elementals.png'},
    Disrupting_Ray: {img: 'https://heroes.thelazy.net/images/8/85/Disrupting_Ray.png'},
    Magic_Arrow: {img: 'https://heroes.thelazy.net/images/4/44/Magic_Arrow.png'},
    Fire_Wall: {img: 'https://heroes.thelazy.net/images/8/8b/Fire_Wall.png'}
};
