'use strict';

const MTOutcast = {
    bannedTowns: null,
    bannedHeroes: [
        'Sorsha', 'Sylvia', 'Rion', 'Sanya',  // Castle
        'Kyrre', 'Malcom',  // Rampart
        'Serena',  // Tower
        'Ash',  // Inferno
        'Xsi', 'Nimbus',  // Necropolis
        'Darkstorn', 'Gunnar',  // Dungeon
        'Dessa', 'Oris', // Stronghold
        'Voy', 'Tiva',  // Fortress
        'Elmore', 'Miriam', 'Astra'  // Cove
    ],
    mirror: true
};
