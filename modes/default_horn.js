'use strict';

const defaultHotAGame = {
    bannedTowns: null,
    bannedHeroes: [
        'Catherine', 'Roland', 'Sir_Mullich',  // Castle
        'Gelu', 'Thorgrim', // Rampart
        'Dracon',  // Tower
        'Xeron',  // Inferno
        'Lord_Haart_the_Death_Knight',  // Necropolis
        'Mutare_Drake',  // Dungeon
        'Boragus', 'Kilgor',  // Stronghold
        'Adrienne',  // Fortress
        'Bidley', 'Tark'  // Cove
    ],
    mirror: null
};
