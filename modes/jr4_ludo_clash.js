'use strict';

const JR4LudoClash = {
    bannedTowns: null,
    bannedHeroes: [
        'Sylvia', 'Cuthbert', 'Sanya', 'Sir_Mullich',  // Castle
        'Thorgrim', 'Coronius', 'Malcom',  // Rampart
        'Thane', 'Iona', 'Serena', 'Aine',  // Tower
        'Rashka', 'Octavia', 'Xyron', 'Ash',  // Inferno
        'Nimbus', 'Xsi',  // Necropolis
        'Synca', 'Geon',  // Dungeon
        'Zubin', 'Oris',  // Stronghold
        'Voy', 'Merist', 'Tiva',  // Fortress
        'Kalt',  // Conflux
        'Leena', 'Astra', 'Elmore'  // Cove
    ],
    mirror: null
};
