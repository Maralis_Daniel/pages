'use strict';

const JR4ShadowEconomy = {
    bannedTowns: null,
    bannedHeroes: [
        'Beatrice', 'Sylvia', 'Cuthbert', 'Sanya', 'Sir_Mullich',  // Castle
        'Giselle', 'Mephala', 'Kyrre', 'Coronius', 'Malcom', 'Gelu', 'Thorgrim', // Rampart
        'Thane', 'Iona', 'Serena', 'Cyra', 'Aine', 'Dracon',  // Tower
        'Rashka', 'Marius', 'Octavia', 'Calh', 'Xyron', 'Ash',  // Inferno
        'Nimbus', 'Thant', 'Xsi', 'Lord_Haart_the_Death_Knight',  // Necropolis
        'Gunnar', 'Synca', 'Alamar', 'Jeddite', 'Geon', 'Deemer',  // Dungeon
        'Shiva', 'Crag_Hack', 'Dessa', 'Terek', 'Gundula', 'Oris', 'Kilgor',  // Stronghold
        'Tazar', 'Broghild', 'Voy', 'Merist', 'Tiva',  // Fortress
        'Kalt', 'Brissa', 'Grindan',  // Conflux
        'Miriam', 'Elmore', 'Eovacius', 'Corkes', 'Tark'  // Cove
    ],
    mirror: null
};
