'use strict';

const JO_UA = {
    bannedTowns: null,
    bannedHeroes: [
        'Sylvia', 'Beatrice', 'Valeska', 'Sanya', 'Edric', 'Christian', 'Rion', 'Cuthbert', 'Ingham', 'Sir_Mullich',  // Castle
        'Giselle', 'Mephala', 'Kyrre', 'Coronius', 'Malcom', 'Clancy', 'Uland', 'Gem', 'Melodia', 'Alagar', 'Gelu', // Rampart
        'Thane', 'Neela', 'Torosar', 'Fafner', 'Iona', 'Serena', 'Aine', 'Theodorus', 'Dracon',  // Tower
        'Rashka', 'Octavia', 'Calh', 'Xyron', 'Ash', 'Pyre', 'Axsis', 'Olema', 'Calid', 'Xarfax',  // Inferno
        'Septienna', 'Nimbus', 'Thant', 'Moandor', 'Charna', 'Ranloo', 'Xsi', 'Lord_Haart_the_Death_Knight',  // Necropolis
        'Gunnar', 'Synca', 'Shakti', 'Alamar', 'Jeddite', 'Geon', 'Arlach', 'Jaegar', 'Darkstorn',  // Dungeon
        'Oris', 'Gretchin', 'Crag_Hack', 'Dessa', 'Terek', 'Gundula', 'Yog', 'Gird', 'Vey', 'Zubin', 'Kilgor',  // Stronghold
        'Voy', 'Drakon', 'Tazar', 'Tiva', 'Korbac', 'Gerwulf', 'Rosic', 'Verdish', 'Merist',  // Fortress
        'Grindan', 'Pasis', 'Thunar', 'Ignissa', 'Brissa', 'Lacus', 'Kalt',  // Conflux
        'Elmore', 'Corkes', 'Cassiopeia', 'Miriam', 'Leena', 'Dargem', 'Bidley', 'Tark'  // Cove
    ],
    mirror: false
};
