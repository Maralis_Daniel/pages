'use strict';

const defaultSODGame = {
    bannedTowns: ['Cove'],
    bannedHeroes: [
        'Catherine', 'Roland', 'Sir_Mullich',  // Castle
        'Gelu', 'Thorgrim', // Rampart
        'Dracon',  // Tower
        'Xeron',  // Inferno
        'Lord_Haart_the_Death_Knight',  // Necropolis
        'Mutare_Drake',  // Dungeon
        'Boragus', 'Kilgor',  // Stronghold
        'Adrienne'  // Fortress
    ],
    mirror: null
};
