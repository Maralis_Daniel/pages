'use strict';

const JO_282 = {
    bannedTowns: null,
    bannedHeroes: [
        'Beatrice', 'Sylvia', 'Valeska', 'Sanya', 'Roland', 'Sir_Mullich',  // Castle
        'Giselle', 'Mephala', 'Ivor', 'Kyrre', 'Coronius', 'Malcom', 'Gelu', // Rampart
        'Thane', 'Neela', 'Torosar', 'Fafner', 'Iona', 'Serena', 'Cyra', 'Aine', 'Dracon',  // Tower
        'Rashka', 'Ignatius', 'Octavia', 'Calh', 'Xyron', 'Ash',  // Inferno
        'Galthran', 'Septienna', 'Nimbus', 'Thant', 'Lord_Haart_the_Death_Knight',  // Necropolis
        'Gunnar', 'Synca', 'Shakti', 'Alamar', 'Jeddite', 'Geon',  // Dungeon
        'Gretchin', 'Crag_Hack', 'Tyraxor', 'Dessa', 'Terek', 'Gundula', 'Kilgor',  // Stronghold
        'Voy', 'Oris', 'Drakon', 'Wystan', 'Tazar', 'Tiva', 'Brissa',  // Fortress
        'Grindan',  // Conflux
        'Elmore', 'Corkes', 'Derek', 'Anabel', 'Cassiopeia', 'Miriam', 'Bidley', 'Tark'  // Cove
    ],
    mirror: false
};
